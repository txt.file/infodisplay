#!/usr/bin/env bash
# SPDX-License-Identifier: Unlicense
# SPDX-FileCopyrightText: 2023 Vieno Hakkerinen <vieno@hakkerinen.eu>

set -e

sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get --no-install-recommends --yes\
	install	firefox-esr libavcodec-extra openbox xdm xserver-xorg
grep --perl-regexp --quiet --regexp="DisplayManager.\*.autoLogin:\tvagrant"\
       	/etc/X11/xdm/xdm-config ||\
	printf "%s" "DisplayManager.*.autoLogin:\tvagrant" >>\
	/etc/X11/xdm/xdm-config
printf "%s" "xrandr -o right\nexec openbox-session" > ~/.xsession
mkdir -p ~/.config/openbox
printf "%s" "firefox --kiosk https://elon.musk.rip/" > \
	~/.config/openbox/autostart
